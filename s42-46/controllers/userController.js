// ========== IMPORTING AND INITIALIZATION ==========
// NPM
const bcrypt = require ("bcrypt");

// LOCAL
const auth = require ("../auth");
const User = require ("../models/User"); // MODEL
const Product = require ("../models/Product"); // MODEL
const Cart = require ("../models/Cart"); // MODEL
// ==================================================

// ========== FUNCTIONS ==========
// REGISTRATION

module.exports.registerUser = async (registerBody) => {

	let doesEmailExist = await User.find ({email: registerBody.email}).then ((fetchedEmail) => {
		if (fetchedEmail.length > 0) {
			return true;
		} else {
			return false;
		}	
	}).catch (err => {
		console.error (err);
		res.status (500).send ({error: `Query Error`})
	});

	let doesUsernameExist = await User.find ({username: registerBody.username}).then ((fetchedUsername) => {
		if (fetchedUsername.length > 0) {
			return true;
		} else {
			return false;
		}
	}).catch (err => {
		console.error (err);
		res.status (500).send ({error: `Query Error`})
	});

	if (!doesEmailExist && !doesUsernameExist) {
		let newUser = new User ({
			username: registerBody.username,
			email: registerBody.email,
			password: bcrypt.hashSync (registerBody.password, 10),
			firstName: registerBody.firstName,
			lastName: registerBody.lastName,
			address: registerBody.address,
			mobileNo: registerBody.mobileNo
		});

		return newUser.save().then (user => {
			if (user) {
				return (`Successfully registered!`)
			} else {
				return (`Oops! Something went wrong...`)
			}
		}).catch (err => {
			console.error (err);
			res.status (500).send ({error: `Register Error`})
		});
	} else if (doesEmailExist) {
		return (`The email is already in use.`)
	} else {
		return (`The username is already in use.`)
	}
};

// LOGIN

module.exports.loginUser = (loginBody) => {
	return User.findOne ({username: loginBody.username}).then (fetchedUser => {

		if (fetchedUser == null) {
			return (`User does not exist.`);

		} else {
			const isPasswordCorrect = bcrypt.compareSync (loginBody.password, fetchedUser.password);

			if (isPasswordCorrect) {
				return {access: auth.createAccessToken (fetchedUser)};

			} else {
				return (`Password is incorrect.`);
			}
		}
	}).catch (err => {
		console.log (error);
		res.status (500).send ({error: `Login Error`})
	});
};

// GET ALL ACTIVE PRODUCTS

module.exports.retrieveAllActiveProducts = () => {
	return Product.find ({isActive : true}).then (allProducts => {
			return allProducts;
			
		}).catch (err => {
			console.log (error);
			res.status (500).send ({error: `Fetch Error`})
		});
};

// GET SPECIFIC PRODUCT

module.exports.retrieveProduct = (productId) => {
	return Product.findById (productId).then (product => {
			if (product.isActive) {
				return product;

			} else {
				return (`Error 404: Product not found.`);
			}
			
		}).catch (err => {
			console.log (error);
			res.status (500).send ({error: `Fetch Error`});
		});
};

// RETRIEVE USER DETAILS

module.exports.retrieveUserDetails = (username) => {
	return User.findOne ({username : username}, {username : 1, email : 1, firstName : 1, lastName : 1}).then (user => {
			return user;

		}).catch (err => {
			console.log (error);
			res.status (500).send ({error: `Fetch Error`});
		});
};

// SET/UNSET ADMIN STATUS 

module.exports.toggleAdmin = (username, reqBody, userData) => {
	if (userData.isAdmin) {
		let toggleStatus = {
			isAdmin : reqBody.isAdmin
		};
		
		return User.findOneAndUpdate ({username : username}, toggleStatus).then (fetchedUser => {
				if (fetchedUser.isAdmin == true) {
					return (`User ${username} access is set to Admin!`);

				} else if (fetchedUser.isAdmin == false) {
					return (`${username} Admin access has been revoked.`);

				} else {
					return (`Invalid request.`);
				}
				
			}).catch (err => {
					console.log (error);
					res.status (500).send ({error: `Fetch Error`})
				});
	
	} else {
		return Promise.reject (`Unauthorized Access: You are not an Admin.`);
	}
};

// ========== EDIT CART ==========
/*module.exports.editCart = async (itemsInfo, user) => {

	await Cart.findOne ({userId : user.id}).then (fetchedCart => {
		if (fetchedCart == null) {
			fetchedCart.allItems.push (itemsInfo);
		}
	})
}*/

// ==================================================