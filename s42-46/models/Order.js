const mongoose = require ("mongoose");
const userId = require ("./User");
const productId = require ("./Product");

const orderSchema = new mongoose.Schema ({
	userId : {
		type : mongoose.Schema.Types.ObjectId,
		ref : "User",
		required : [true, "User ID is required."]
	},
	products : [
		{
			productId : {
				type : mongoose.Schema.Types.ObjectId,
				ref: "Product"
				required : [true, "Product ID is required."]
			}
		},
		{
			quantity : {
				type : Number,
				default : 0
			}
		}
	],
	totalAmount : {
		type : Number,
		default: 0
	},
	purchasedOn : {
		type : Date,
		default : new Date()
	}
});

// ========== EXPORTING ==========
module.exports = mongoose.model("Order", orderSchema);
// ===============================