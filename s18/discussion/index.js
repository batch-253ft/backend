// console.log("Hello!");

//Functions
	
		//Parameters and Arguments

			// Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
			// Functions are mostly created to create complicated tasks to run several lines of code in succession
			// They are also used to prevent repeating lines/blocks of codes that perform the same task/function

			//We also learned in the previous session that we can gather data from user input using a prompt() window.

function printInput() {
	let nickname = prompt("Enter your nickname: ");
	console.log("Hi, " + nickname + "!");
}

// printInput();

function printName(name) {

	console.log("My name is " + name);
}

// printName("Adrian Tamayo");
// printName("Eren Jaeger");
// printName("Ayanokoji Kiyotaka");

// variables can also be passed as an argument
let sampleName = "Yui";

// printName(sampleName);

function isDivisibleBy8(dividend) {
	let remainder = (dividend % 8);
	let divisibilityCheck = (remainder === 0);
	console.log("The remainder of " + dividend + " is " + remainder);
	console.log("Is " + dividend + " divisible by 8?");
	console.log(divisibilityCheck);
};

// isDivisibleBy8(64);
// isDivisibleBy8(28);


// Functions as Arugments
function argumentFunction() {
	console.log("This function was passed as an argument before the message is printed");
};

function invokeFunction(argumentFunction) {
	argumentFunction();
};

// invokeFunction(argumentFunction);


// Multiple Parameters
function createFullName(givenName, middleName, surName) {

	console.log(givenName + " " + middleName + " " + surName);
};

createFullName("Juan", "Dela", "Cruz");
createFullName("Juan", "Dela"); // if arguments is less than the number of parameters, remaining parameters will return as undefined
createFullName("Juan", "Dela", "Cruz", "Hello"); // if argument is more than the number of parameters, the exceeding argument will not display on the console

let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

//The order of the argument is the same to the order of the parameters. The first argument will be stored in the first parameter, second argument will be stored in the second parameter and so on.

// The Return Statement
// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function.

function returnFullName(firstName, middleName, lastName) {

	return firstName + " " + middleName + " " + lastName;
	console.log("This message will not display.");

	// Notice that the console.log() after the return is no longer printed in the console that is because ideally any line/block of code that comes after the return statement is ignored because it ends the function execution.
};

let completeName = returnFullName("Lucy", " ", "Pevensie");
console.log(completeName);

console.log(returnFullName());

function returnAddress(city, country) {

	let fullAddress = city + ", " + country;
	return fullAddress;
};

let address = returnAddress("Shire", "Middle Earth");
console.log(address);

function printPlayerInfo(username, level, jobClass) {

	// console.log("Usermame: " + username);
	// console.log("Level: " + level);
	// console.log("Job: " + jobClass);

	return "Username: " + username + "\n" + "Level: " + level + "\n" + "Job: " + jobClass;
};

let user1 = printPlayerInfo("e19hty", 70, "Assassin");
console.log(user1);

//returns undefined because printPlayerInfo returns nothing. It only console.logs the details. 

//You cannot save any value from printPlayerInfo() because it does not return anything.