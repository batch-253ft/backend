// console.log("Hello World!");

// Add 2 Numbers

function addNum(num1, num2) {
	console.log("Displayed sum of " + num1 + " and " + num2);
	console.log(num1 + num2);
};

addNum(5, 15);

// Subtract 2 Numbers

function subNum(num1, num2) {
	console.log("Displayed difference of " + num1 + " and " + num2);
	console.log(num1 - num2);
};

subNum(20, 5);

// Multiply 2 Numbers

function multiplyNum(num1, num2) {

	console.log("The product of " + num1 + " and " + num2 + ":");
	return (num1 * num2);
};

let product = multiplyNum(50, 10);
console.log(product);

// Divide 2 Numbers

function divideNum(num1, num2) {

	console.log("The quotient of " + num1 + " and " + num2 + ":");
	return (num1 / num2);
};

let quotient = divideNum(50, 10);
console.log(quotient);

// Calculate Area of Circle

function getCircleArea(radius) {

	console.log("The result of getting the area of a circle with " + radius + " radius:");
	return (3.1416 * [radius ** 2]);
};

let circleArea = getCircleArea(15);
console.log(circleArea);

// Calculate Average of 4 Numbers

function getAverage(num1, num2, num3, num4) {

	console.log("The average of " + num1 + "," + num2 + "," + num3 + " and " + num4 + ":");
	return ([num1 + num2 + num3 + num4] / 4);
};

let averageVar = getAverage(20, 40, 60, 80);
console.log(averageVar);

// Checking if Score Passed

function checkifPassed(yourScore, totalScore) {

	console.log("Is " + yourScore + "/" + totalScore + " a passing score?");
	let scorePercentage = ([yourScore/totalScore] * 100);
	return isPassed = scorePercentage >= 75;
};

let isPassingScore = checkifPassed(38, 50);
console.log(isPassingScore);

//Do not modify
//For exporting to test.js
try {
	module.exports = {
		addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
	}
} catch (err) {

}