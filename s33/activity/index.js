// console.log ("Hello World!");

async function getData () {

	let response = await fetch ("https://jsonplaceholder.typicode.com/todos/", {method: "GET"});
	let fetchedData = await (response.json());
	console.log (fetchedData);

	let titleArray = fetchedData.map ((post) => {
			return post.title;
	});

	console.log (titleArray);
};

getData ();