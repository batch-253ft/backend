// console.log("Hello World");

// Login
function login(username, password, role) {
	if (username == "" || username == undefined || password == "" || password == undefined || role == "" || role == undefined) {
		return 'Inputs must not be empty';
	} else {
		switch(role) {
			case "admin":
				return 'Welcome back to the class portal, admin!'
				break;
			case "teacher":
				return 'Thank you for logging in, teacher!'
				break;
			case "student":
				return 'Welcome to the class portal, student!'
				break;
			default:
				return 'Role out of range.'
				break;
		}
	}
};


// Grade Average and Equivalent 

function checkAverage(score1, score2, score3, score4) {
	let average = ((score1 + score2 + score3 + score4) / 4);
	average = Math.round(average);
	
	if (average <= 74) {
		return "Hello, student, your average is " + average + ". The letter equivalent is F"
	} else if (average >= 75 && average <= 79) {
		return "Hello, student, your average is " + average + ". The letter equivalent is D"
	} else if (average >= 80 && average <= 84) {
		return "Hello, student, your average is " + average + ". The letter equivalent is C"
	} else if (average >= 85 && average <= 89) {
		return "Hello, student, your average is " + average + ". The letter equivalent is B"
	} else if (average >= 90 && average <= 95) {
		return "Hello, student, your average is " + average + ". The letter equivalent is A"
	} else if (average >= 96) {
		return "Hello, student, your average is " + average + ". The letter equivalent is A+"
	}
};

