// Setup dependencies
const express = require ("express");
const mongoose = require ("mongoose");

// This allows us to use all the routes defined in "taskRoute.js"
const taskRoute = require ("./routes/taskRoute")

// Server setup
const app = express ();
const port = 4000;
// Middlewares
app.use (express.json ());
app.use (express.urlencoded ({extended: true}));

// Database connection
// Connecting to MongoDB Atlas
mongoose.connect ("mongodb+srv://admin:admin123@batch253-tamayo.wevsze0.mongodb.net/s36?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
);

let db = mongoose.connection;

db.on ("error", console.error.bind (console, "connection error"));

db.once ("open", () => console.log (`We're now connected to the cloud database: MongoDB Atlas`));

app.use ("/tasks", taskRoute);

if (require.main === module) {
	app.listen (port, () => console.log (`Server running at port ${port}`));
}

module.exports = app;