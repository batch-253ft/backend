// console.log("Hello!");

// [SECTION] Arithmetic Operators
let x = 81, y = 9;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y; // asterisk for multiplication
console.log("Result of multiplication operator: " + product);

let quotient = x / y; // forwards slash for division
console.log("Result of division operator: " + quotient);

let remainder = x % y; // percentage for remainder
console.log("Result of modulo operator: " + remainder);


// [SECTION] Assignment Operators
// Basic Assignment Operator (=)
// The assignment operator assigns the value of the **right** operand to a variable.

let assignmentNumber = 8;

// Addition Assignment Operator
// assignmentNumber = assignmentNumber + 2;
// console.log("Result of addition assignment operator: " + assignmentNumber)

// shorthand
assignmentNumber += 2;
console.log(assignmentNumber);

/*

	Mini-activity:
		1. Use assignment operators to other arithmetic operators.
		2. Console log the results

*/

/*let miniActivityNumber = 10;

// subtraction
miniActivityNumber -= 5;
console.log(miniActivityNumber);

// multiplication
miniActivityNumber *= 5;
console.log(miniActivityNumber);

// division
miniActivityNumber /= 5;
console.log(miniActivityNumber);

// remainder
miniActivityNumber %= 5;
console.log(miniActivityNumber);*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of mdas: " + pemdas);


// Increment and Decrement
// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied
let z = 1;

let increment = ++z; //pre-increment
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

increment = z++; //post-increment
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " + z);

// ++z returns the value of z after it has been incremented. z++ returns the value of z before incrementing. When the ++ comes before its operand it is called the "pre-increment" operator, and when it comes after it is called the "post-increment" operator.


let decrement = --z; //pre-decrement
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

decrement = z--; //post-decrement
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z);

// [SECTION] Type Coercion

/*
            - Type coercion is the automatic or implicit conversion of values from one data type to another
            - This happens when operations are performed on different data types that would normally not be possible and yield irregular results
            - Values are automatically converted from one data type to another in order to resolve operations

*/

let numA = '10';
let numB = 12;
let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16, numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// Discrete math
let numE = true + 1;
console.log(numE);

let numF = false + 1;
console.log(numF);

// [SECTION] Comparison Operators
// Loose Equality Operator (==)
/* 
    - Checks whether the operands are equal/have the same content
    - Attempts to CONVERT AND COMPARE operands of different data types
    - Returns a boolean value
*/

let juan = "juan";

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1'); //loose equality
console.log("juan" == "juan");
console.log(juan == "juan"); //compares only value

// Inequality operator (!=) (loose inequality)
/* 
            - Checks whether the operands are not equal/have different content
            - Attempts to CONVERT AND COMPARE operands of different data types
*/

console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1'); 
console.log("juan" != "juan");
console.log(juan != "juan"); 


// Strict Equality Operator (===) (strict equality)
/* 
    - Checks whether the operands are equal/have the same content
    - Also COMPARES the data types of 2 values
    - JavaScript is a loosely typed language meaning that values of different data types can be stored in variables
    - In combination with type coercion, this sometimes creates problems within our code (e.g. Java, Typescript)
    - Some programming languages require the developers to explicitly define the data type stored in variables to
	- Some programming languages require the developers to explicitly define the data type stored in variables to prevent this from happening
    - Strict equality operators are better to use in most cases to ensure that data types provided are correct
*/

console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1');
console.log(0 === false); 
console.log("juan" === "juan");
console.log(juan === "juan"); 
console.log(null === undefined);

// Strict Inequality Operator (!==)
/* 
    - Checks whether the operands are not equal/have the same content
    - Also COMPARES the data types of 2 values
*/

console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(0 !== false); 
console.log("juan" !== "juan");
console.log(juan !== "juan"); 
console.log(null !== undefined);

// Relational Operators
// Some comparison operators check whether one value is greater or less than to the other value.
// Has a boolean value

let a = 50;
let b = 65;

// GT (>)
let isGreaterThan = a > b;
// LT (<)
let isLessThan = a < b;
// GTE (>=)
let isGTorEqual = a >= b;
// LTE (<=)
let isLTorEqual = a <= b;

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTorEqual);
console.log(isLTorEqual);

console.log(a > '30'); // forced coercion to change the string data type to a number
console.log(a > 'twenty');

let str = 'forty';
console.log(b >= str); //NaN - not a number

// [SECTION] Logical Operators
let isLegalAge = true;
let isRegistered = false;

// Logical AND operator (&& - double ampersand)
// Returns true if all operands are true (1 * 0 = 1)
let areAllRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND operator: " + areAllRequirementsMet);

// Logical OR operator (|| - double pipe)
// Returns true if one of the operands are true (1 + 0 = 1)
let areSomeRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical AND operator: " + areSomeRequirementsMet);

// Logical NOT operator (! - exclamation point)
// Returns the opposite value
let areSomeRequirementsNotMet =! isRegistered;
console.log("Result of logical NOT operator: " + areSomeRequirementsNotMet);