const express = require ("express");
const router = express.Router();

const courseController = require ("../controllers/courseController");
const auth = require ("../auth");

// Route for creating a course

router.post ("/", auth.verify, (req, res) => {

	const userData = auth.decode (req.headers.authorization);

	if(userData.isAdmin){
		courseController.addCourse (req.body).then (resultFromController => res.send (resultFromController)).catch (err => res.send(err));
	} else {
		res.send(false);
	}

// ====== MY SOLUTION ======
	/*const isAdminData = auth.decode (req.headers.authorization).isAdmin;

	courseController.addCourse (req.body, isAdminData).then (resultFromController => res.send (resultFromController)).catch (err => res.send (err));*/
});

// Route for retrieving all the courses
router.get ("/all", auth.verify, (req, res) => {  // need middleware auth

	const userData = auth.decode (req.headers.authorization);

	if (userData.isAdmin){
		courseController.getAllCourses().then (resultFromController => res.send (resultFromController)).catch (err => res.send (err));
	} else {
		res.send(false);
	}
});

// Route for retrieving all the ACTIVE courses
// Middleware for verifying JWT is not required because users who aren't logged in should also be able to view the courses

router.get ("/", (req, res) => {

	courseController.getAllActive().then (resultFromController => res.send (resultFromController)).catch (err => res.send (err));
});

// Route for retrieving a specific course
// Creating a route using the "/:parameterName" creates a dynamic route, meaning the url is not static and changes depending on the information provided in the url

router.get ("/:id", (req, res) => {

	// Since the course ID will be sent via the URL, we cannot retrieve it from the request body
	// We can however retrieve the course ID by accessing the request's "params" property which contains all the parameters provided via the url
	// Example: URL - http://localhost:4000/courses/613e926a82198824c8c4ce0e
		// The course Id is "613e926a82198824c8c4ce0e" which is passed via the url that corresponds to the "courseId" in the route

	courseController.getCourse(req.params.id).then (resultFromController => res.send (resultFromController)).catch (err => res.send (err));
});

// Route for updating a course
// JWT verification is needed for this route to ensure that a user is logged in before updating a course

router.put ("/:id", auth.verify, (req, res) => {
	const userData = auth.decode (req.headers.authorization);

	if (userData.isAdmin) {
		courseController.updateCourse (req.params.id, req.body).then (resultFromController => res.send (resultFromController)).catch (err => res.send (err));
	} else {
		res.send (false);
	}
});

// A "PATCH" request is used instead of "DELETE" request because of our approach in archiving and hiding the courses from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases

router.patch ("/:id/archive", auth.verify, (req, res) => {
	const userData = auth.decode (req.headers.authorization);

	if (userData.isAdmin) {
		courseController.archiveCourse (req.params.id, req.body).then (resultFromController => res.send (resultFromController)).catch (err => res.send (err));
	} else {
		res.send (false);
	}
});

// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;