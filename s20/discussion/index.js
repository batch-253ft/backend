// console.log("Annyeong!");

// LOOPS
/*
	- A while loop takes in an expression/condition
	- Expressions are any unit of code that can be evaluated to a value
	- If the condition evaluates to true, the statements inside the code block will be executed
	- A statement is a command that the programmer gives to the computer
	- A loop will iterate a certain number of times until an expression/condition is met
	- "Iteration" is the term given to the repetition of statements

	Syntax:
		while(expression/condition) {
			statement;
		}
*/
/*
let count = 5;

// while the value of count is not equal to 0
while(count !==0) {

	// The current value of count is printed out
	console.log("while: " + count);
	count--;
}*/

///////////////////////////////////////////////////////////////////////////////
/*
	MINI-ACTIVITY

	Create a while loop if the "i" variable is less than or equal to 15, display "Hi <i>"



let i = 0;

while(i <= 15) {
	console.log("Hi " + i);
	i++;
}*/

///////////////////////////////////////////////////////////////////////////////
// Do-While Loop
/*
	- A do-while loop works a lot like the while loop. But unlike while loops, do-while loops guarantee that the code will be executed at least once.
	
	Syntax:
		do {
			statement;
		} while (expression/condition)

*/

/*let number = Number(prompt("Give me a number"));

do {
	// The current value of a number is printed out
	console.log("Do while " + number);

	// Increases the value of number by 1 after every iteration to stop the loop when it reaches 10 or greater
	number += 1;
} while (number < 10)*/

///////////////////////////////////////////////////////////////////////////////
// For Loop
/*
	- A for loop is more flexible than while and do-while loops. It consists of three parts:
	    1. The "initialization" value that will track the progression of the loop.
	    2.  The "expression/condition" that will be evaluated which will determine whether the loop will run one more time.
	    3. The "iteration" indicates how to advance the loop.

	    Syntax:
			for(initialization/initial value; expression/condition; iteration) {
				statement;
			}
*/
/*
for(let n = 0; n <= 20; n++) {

	// The current value of n is printed out
	console.log(n);
}*/

/*let myString = "IAmADeveloper";

// .length property is a number data type
console.log(myString.length); // 13

console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);

for(let x = 0; x < myString.length; x++) {

	console.log(myString[x]);
}*/

/*
	MINI-ACTIVITY

	create a for loop that will display use your given name as values and length and count the vowels
*/
let myName = "Adrian";
let vowelCount = 0;

/*for(let x = 0; x < myName.length; x++) {
	let letter = myName[x].toLowerCase();
	if (letter == "a" || letter == "e" || letter == "i" || letter == "o" || letter == "u") {
		vowelCount++;
	}
}

console.log("Number of vowels in myName: " + vowelCount);*/

/*for(let i = 0; i < myName.length; i++) {
	// If the character of your name is a vowel letter, instead of displaying the character, display "*"
	// The ".toLowerCase()" function/method will change the current letter being evaluated in a loop to a lowercase letter ensuring that the letters provided in the expressions below will match
	if (
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 
		) 
	{

		console.log("*");
	} else {

		console.log(myName[i]);
	}
}*/

for (let count = 0; count <= 20; count++) {
	if (count % 2 === 0) {

		continue;
	}

	console.log ("Continue and Break: " + count)

	if (count > 10) {
		break;
	}
}

let name = "alejandro";

for (let i = 0; i < name.length; i++) {

	console.log (name[i]);

	if (name[i].toLowerCase() === "a") {
		console.log ("Continue to the next iteration");
		continue;
	}

	if (name[i] == "d") {
		break;
	}
}